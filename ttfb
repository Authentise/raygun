#!/usr/bin/env python3
import asyncio
import contextlib
import collections
import logging
import requests
import statistics
import sys
import time

import diax.aioclient
import diax.log
import diax.scripting

LOGGER = logging.getLogger('ttfb')

class Timer():
    def __init__(self):
        self.start = None
        self.end = None

    def elapsed(self):
        return round(self.end - self.start, 3)

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *exc_details):
        self.end = time.time()
        LOGGER.debug("%s seconds", self.elapsed())

def show_stats(stats):
    LOGGER.info("Finished %d requests", stats.maxlen)
    LOGGER.info("Mean: %s", statistics.mean(stats))
    LOGGER.info("Median: %s", statistics.median(stats))
    LOGGER.info("Variance: %s", statistics.variance(stats))

async def _run(args, client, stats):
    while True:
        for _ in range(stats.maxlen):
            with Timer() as t:
                response, _ = await client.get('users', '/sessions/')
            stats.append(t.elapsed())
        show_stats(stats)

def main():
    logging.getLogger('diax.aioclient').setLevel(logging.INFO)

    parser = diax.scripting.parser()
    parser.add_argument('-n', '--number', type=int, default=1, help="Number of concurrent requests to make")
    args = parser.parse_args()
    client = diax.aioclient.create(args)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(client.login())
    stats = collections.deque(maxlen=100)
    loop.create_task(_run(args, client, stats))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        LOGGER.info("Shutdown")
        show_stats(stats)
    return 0

if __name__ == '__main__':
    sys.exit(main())
